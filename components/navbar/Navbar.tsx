import React, { FC } from 'react'
import Link from 'next/link';


export const Navbar:FC = () => {
  return (
    <nav className="bg-secondary-100">

      <div className="mx-auto px-2 sm:px-6 lg:px-8">

        <div className="relative flex items-center justify-between h-16">

          {/* logotipo */}
          <div className="flex-1 flex items-center justify-start">

            <div className="logo-nav flex items-center px-7">
              <span className="text-white text-6xl">V</span>
              <span className="text-white text-3xl font-bold">Hunter</span>
            </div>

          </div>

          {/* menu admin */}
          <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">

            <div className="ml-3 relative">
                <button type="button" className="menu-nav bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
                    <span className="sr-only">Open user menu</span>
                    <img className="h-8 w-8 rounded-full" src="/user-circle.svg" alt=""/>
                </button>

                <div className="menu-dropdown origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabIndex={-1}>

                    <Link passHref href="#">
                      <a className="block px-4 py-2 text-sm text-gray-700 hover:bg-primary-100" role="menuitem" tabIndex={-1} id="user-menu-item-0">Add Product</a>
                    </Link>
                    <Link passHref href="#">
                      <a className="block px-4 py-2 text-sm text-gray-700 hover:bg-primary-100" role="menuitem" tabIndex={-1} id="user-menu-item-1">Add Component</a>
                    </Link>
                    <Link passHref href="#">
                      <a className="block px-4 py-2 text-sm text-gray-700 hover:bg-primary-100" role="menuitem" tabIndex={-1} id="user-menu-item-2">Sign out</a>
                    </Link>
                </div>
            </div>

          </div>

        </div>

      </div>

    </nav>
  )
}

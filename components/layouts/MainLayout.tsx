import React, { FC, ReactNode } from 'react'
import Head from 'next/head'
import { Navbar } from '../navbar/Navbar';


interface Props {
  title?: string;
  children: ReactNode;
}

export const MainLayout: FC<Props> = ( { children, title } ) => {
  return (
    <>
    
      <Head>
        <title>{ title || 'Vhunter' }</title>
        <meta name='description' content='Cazador de versiones' />
        <meta name='keywords' content='crawler, version, producto, componente' />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Navbar/>

      <main>
        {children}
      </main>

    </>
  )
}

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
    colors: {
      primary: {
        100: "#21DBAA",
        200: "#2FC191",
        300: "#478B77",
      },
      secondary: {
        100: "#0C0C0C",
        200: "#191919",
        300: "#202020",
        400: "#333333",
        500: "#4a4a4a",
        600: "#686868",
        700: "#DDDDDD",
        800: "#EFEFEF",
      },
      third: "#A74449",
      white: "#ffffff",
    },
  },
  plugins: [],
};

import type { NextPage } from "next";
import Link from "next/link";
import { MainLayout } from '../components/layouts/MainLayout';

const Home: NextPage = () => {
  return (
    <MainLayout>
      <div className='text-center mt-5'>
        <h1 className='text-3xl font-bold underline'>Hello mundo!</h1>
        <Link href={"/about"}>Ir a about</Link>
      </div>
    </MainLayout>
  );
};

export default Home;


